This repository is the source code for the paper “**Evidence for
multiple subpopulations of herpesvirus-latently infected cells**”

All raw data generated from seven bridges may be found in `data/counts`
for cellular genes and `data/virus` for KSHV genes. The scripts
`/data/Collect_UMI_(Cell|kshv)_Counts.R` are used to generate the inputs
used as the initial assays for the `SingleCellExperiment` objects stored
in `cellular/Cellular_SingleCellExperiment.rds` and
`kshv/KSHV_SingleCellExperiment.rds`.

The `analysis` directory contains all of the main analysis and figure
generations.

The `R` directory contains helpful functions scripts.
